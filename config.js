var config = {};

// www.inberlinwohnen.de

// //  TEMPLATE
// {
//     name: 'StadtUndLand',
//     urls: [
//         'https://www.stadtundland.de/immobiliensuche.php?form=stadtundland-expose-search-1.form&sp%3Acategories%5B3352%5D%5B%5D=-&sp%3Acategories%5B3352%5D%5B%5D=__last__&sp%3AroomsFrom%5B%5D=&sp%3AroomsTo%5B%5D=&sp%3ArentPriceFrom%5B%5D=&sp%3ArentPriceTo%5B%5D=700&sp%3AareaFrom%5B%5D=35&sp%3AareaTo%5B%5D=&sp%3Afeature%5B%5D=__last__&action=submit'
//     ],
//     baseUrl: '',
//     linkPrefix: '',
//     searchKeyWord: 'stadtundland',
//     get fileName() {
//         return 'results' + this.name + '.ids';
//     }
// },

// https://www.ivd24immobilien.de/search-photon/?osm_id=240109189&osm_key=place&osm_value=city&osm_postcode=10117&osm_lat=52.5170365&osm_long=13.3888599&nutzungsart_id=&vermarktungsart_id=10000000010&5eb2b57ce3c7c=Berlin&search_term=Berlin&objektart_id=2&anzahl_zimmer=&preis_bis=700&groesse_ab=40&radius=0

config.sources = [
    // {
    //     name: 'ivd24immobilien',
    //     urls: [
    //         'https://www.ivd24immobilien.de/search-photon/?osm_id=240109189&osm_key=place&osm_value=city&osm_postcode=10117&osm_lat=52.5170365&osm_long=13.3888599&nutzungsart_id=&vermarktungsart_id=10000000010&5eb2b57ce3c7c=Berlin&search_term=Berlin&objektart_id=2&anzahl_zimmer=&preis_bis=700&groesse_ab=40&radius=0'
    //     ],
    //     baseUrl: '',
    //     linkPrefix: '',
    //     searchKeyWord: 'ivd24immobilien',
    //     get fileName() {
    //         return 'results' + this.name + '.ids';
    //     }
    // },
    // {
    //     name: 'immoNet',
    //     urls: [
    //         'https://www.immonet.de/immobiliensuche/sel.do?&sortby=0&suchart=1&objecttype=1&marketingtype=2&parentcat=1&toprice=700&fromarea=40&city=87372&locationname=Berlin'
    //     ],
    //     baseUrl: 'https://www.immonet.de',
    //     linkPrefix: '',
    //     searchKeyWord: 'angebot',
    //     get fileName() {
    //         return 'results' + this.name + '.ids';
    //     }
    // },
    // {
    //     name: 'ImmoWelt',
    //     urls: [
    //         'https://www.immowelt.de/liste/berlin/wohnungen/mieten?prima=700&wflmi=40&sort=relevanz'
    //     ],
    //     baseUrl: 'https://www.immowelt.de',
    //     linkPrefix: '',
    //     searchKeyWord: 'expose',
    //     get fileName() {
    //         return 'results' + this.name + '.ids';
    //     }
    // },
    {
        name: 'Immoscout',
        urls: [
            'https://www.immobilienscout24.de/Suche/shape/wohnung-mieten?shape=bXZoX0l7emtwQX5xQmttR2JdZ2lIZ3dAcXVFbWtCb2NAd31AY1B5d0FhXXdsQGxFZVx2VVlpQGdEbkVhTHhLeVdnQ3NObnBBd3lAYntAdWhCYHNDaUBkc0R2aEB8eEBic0RyZUBocUNqfkZoYEFfTnpkQH5NbElvY0A.&price=-550.0&livingspace=40.0-&enteredFrom=saved',
        ],
        baseUrl: 'https://www.immobilienscout24.de',
        linkPrefix: '/expose/',
        searchKeyWord: 'expose',
        get fileName() {
            return 'results' + this.name + '.ids';
        }
    },
    {
        name: 'Gewobag',
        urls: [
            'https://www.gewobag.de/fuer-mieter-und-mietinteressenten/mietangebote/?bezirke%5B%5D=charlottenburg-wilmersdorf&bezirke%5B%5D=charlottenburg-wilmersdorf-charlottenburg&bezirke%5B%5D=friedrichshain-kreuzberg&bezirke%5B%5D=friedrichshain-kreuzberg-friedrichshain&bezirke%5B%5D=lichtenberg&bezirke%5B%5D=mitte&bezirke%5B%5D=mitte-moabit&bezirke%5B%5D=neukoelln&bezirke%5B%5D=neukoelln-neukoelln&bezirke%5B%5D=pankow&bezirke%5B%5D=pankow-prenzlauer-berg&bezirke%5B%5D=reinickendorf&bezirke%5B%5D=reinickendorf-reinickendorf&bezirke%5B%5D=reinickendorf-waidmannslust&bezirke%5B%5D=tempelhof-schoeneberg&bezirke%5B%5D=tempelhof-schoeneberg-lichtenrade&bezirke%5B%5D=tempelhof-schoeneberg-schoeneberg&nutzungsarten%5B%5D=wohnung&gesamtmiete_von=&gesamtmiete_bis=700&gesamtflaeche_von=35&gesamtflaeche_bis=&zimmer_von=&zimmer_bis=&keinwbs=1'
        ],
        baseUrl: '',
        linkPrefix: '',
        searchKeyWord: 'mietangebote',
        get fileName() {
            return 'results' + this.name + '.ids';
        }
    },
    {
        name: 'WBM',
        urls: [
            'https://www.wbm.de/wohnungen-berlin/angebote/'
        ],
        baseUrl: 'https://www.wbm.de',
        linkPrefix: '',
        searchKeyWord: 'details',
        get fileName() {
            return 'results' + this.name + '.ids';
        }
    },
    {
        name: 'Gesobau',
        urls: [
            'https://www.gesobau.de/mieten/wohnungssuche.html?list%5BzimmerMin%5D=&list%5BflaecheMin%5D=35&list%5BmieteMax%5D=700'
        ],
        baseUrl: 'https://www.gesobau.de',
        linkPrefix: '',
        searchKeyWord: 'wohnung/',
        get fileName() {
            return 'results' + this.name + '.ids';
        }
    },
    {
        name: 'Degewo',
        // not verified if working
        urls: [
            'https://immosuche.degewo.de/de/search?size=10&page=1&property_type_id=1&categories%5B%5D=1&lat=&lon=&area=&address%5Bstreet%5D=&address%5Bcity%5D=&address%5Bzipcode%5D=&address%5Bdistrict%5D=&address%5Braw%5D=&district=&property_number=&price_switch=true&price_radio=700-warm&price_from=&price_to=&qm_radio=20&qm_from=&qm_to=&rooms_radio=null&rooms_from=&rooms_to=&wbs_required=false&order=rent_total_without_vat_asc'
        ],
        baseUrl: '',
        linkPrefix: '',
        searchKeyWord: 'degewo.de',
        get fileName() {
            return 'results' + this.name + '.ids';
        }
    },
    {
        // does currently not work )-:
        name: 'Howoge',
        urls: [
            'https://www.howoge.de/wohnungen-gewerbe/wohnungssuche.html?tx_howsite_json_list%5Brent%5D=700&tx_howsite_json_list%5Barea%5D=35&tx_howsite_json_list%5Brooms%5D=egal&tx_howsite_json_list%5Bwbs%5D=wbs-not-necessary',
            // 'https://www.howoge.de/wohnungen-gewerbe/wohnungssuche.html'
        ],
        baseUrl: 'https://www.howoge.de',
        linkPrefix: '',
        searchKeyWord: 'detail',
        // /wohnungen-gewerbe/wohnungssuche/detail/7532.html
        get fileName() {
            return 'results' + this.name + '.ids';
        }
    },
    {
        name: 'StadtUndLand',
        urls: [
            'https://www.stadtundland.de/immobiliensuche.php?form=stadtundland-expose-search-1.form&sp%3Acategories%5B3352%5D%5B%5D=-&sp%3Acategories%5B3352%5D%5B%5D=__last__&sp%3AroomsFrom%5B%5D=&sp%3AroomsTo%5B%5D=&sp%3ArentPriceFrom%5B%5D=&sp%3ArentPriceTo%5B%5D=700&sp%3AareaFrom%5B%5D=35&sp%3AareaTo%5B%5D=&sp%3Afeature%5B%5D=__last__&action=submit'
        ],
        baseUrl: '',
        linkPrefix: '',
        searchKeyWord: 'stadtundland',
        get fileName() {
            return 'results' + this.name + '.ids';
        }
    }
];

module.exports = config;