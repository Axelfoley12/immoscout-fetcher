
var request = require('request');
var cheerio = require('cheerio');
var fs = require('fs');
var opn = require('opn');

// load config and environment variables
require('dotenv').config();
var config = require('./config');

const sources = config.sources;


function doRequest(url) {
    return new Promise(function (resolve, reject) {
            request(url, function (err, resp, body) {
                if ( typeof body !== 'undefined' && body ) {
                    $ = cheerio.load(body);
                    links = $('a'); //jquery get all hyperlinks
                    resolve(links);
                } else {
                    console.log("Body seems undefined!", {
                        url: url
                    });
                }
            });
    });
}

function openLinks (source, newIDs) {
    console.log(new Date() + " --> new IDs for" + source.name);
    let IDs = Object.keys(newIDs);
    IDs.forEach(ID => {
        const completeLink = source.baseUrl + source.linkPrefix + ID;
        console.log("New link: " + completeLink);

        // open link in browser/default browser
        opn(completeLink);
        // opn(completeLink, {app: 'firefox'});
    });
}

async function getLinks(source, url) {
    // parse webpage
    let links = await doRequest(url);
    let newIDs = {};
    
    // check if links fit pattern
    $(links).each(function (i, link) {
        if (!(typeof $(link).attr('href') === "undefined")) {
            // console.log($(link).attr('href'));
            if ($(link).attr('href').indexOf(source.searchKeyWord) > -1) {
                // console.log($(link).attr('href'));
                let linkString = $(link).attr('href');
                let id = linkString.replace(source.linkPrefix, '');
    
                // list of new links
                if (!(id in source.IDHash)) {
                    newIDs[id] = true;
                }
    
                // add links to old list
                source.IDHash[id] = true;
            }
        }
    });

    // open all new IDs in browser
    if (Object.keys(newIDs).length != 0) {
        openLinks(source, newIDs);
    } else {
        console.log(
            new Date().toISOString().replace('T', ' ').substring(0, 19) + 
            " --> nothing new for " + 
            source.name
        );
    }

    // turn into json
    var json = JSON.stringify(source.IDHash);

    // write final JSON to file
    fs.writeFile(source.fileName, json, 'utf8', function writeFileCallback(
        err, res) {
            if (err){
                console.error(err);
            }
        }
    );
}

function checkSource(source) {
    if (fs.existsSync(source.fileName)) {
        // read old file if exists and pass IDs with IDs on website
        fs.readFile(source.fileName, 'utf8', function readFileCallback(err, data) {
            if (err){
                console.error(err);
            } else {
                source.IDHash = JSON.parse(data);
                source.urls.forEach(url => {
                    getLinks(source, url);
                });
            }
        });
    } else {
        // just get the new IDs if it doesnt exist
        console.log(source.fileName + " does not exists");
        
        source.urls.forEach(url => {
            getLinks(source, url);
        });
    }
}

function main() {
    sources.forEach(source => {
        // initialize ID hash
        source.IDHash = {};

        // check for new IDs
        checkSource(source);
    }); 
}


(async function example() {
    main();
})().catch((e) => console.error(e));
