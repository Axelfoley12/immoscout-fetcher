#!/bin/bash

BREAK_TIME=30

PARENT_PATH=$(dirname "$0")

echo "executing in ${PARENT_PATH}"
cd ${PARENT_PATH}
while [ true ]; do
    node ./index.js
    sleep ${BREAK_TIME}
done
